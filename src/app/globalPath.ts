// local path with IP address
// export const baseUrl = 'http://192.168.100.21:5000/';

// local path with ip server path
export const baseUrl = 'http://35.162.247.138:5000/'; // ip link
// socketUrl
export const socketUrl = "ws://35.162.247.138:5000/webSocket";
// export const SAMPLE_SERVER_BASE_URL = 'https://tokboxhireseat.herokuapp.com/'; // Sujith

// Test Server
// export const baseUrl = "http://44.232.150.23:5002/"; //ip link
// socketUrl
// export const socketUrl = "wss://44.232.150.23:5002/webSocket";

//  export const baseUrl = 'http://portfolio.theaxontech.com:5002/'
// export const baseUrl = "http://localhost:5000/"; //local link link
// export const baseUrl = "http://192.168.100.44:5000/"; //local link link
// socketUrl
// export const socketUrl = "wss://192.168.100.44:5000/webSocket";

// live Path with aws ip
// export const baseUrl = 'http://52.36.196.182:5000/';
// socketUrl
// export const socketUrl = "wss://52.36.196.182:5000/webSocket";

// live Path with domain
// export const baseUrl = 'https://hireseat.com:5000/';
// socketUrl
// export const socketUrl = "wss://hireseat.com:5000/webSocket";

// test server path
// export const baseUrl = 'https://hireseat.com:5002/';
// socketUrl
// export const socketUrl = "wss://hireseat.com:5002/webSocket";
